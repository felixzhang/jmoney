/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50610
Source Host           : localhost:3306
Source Database       : money

Target Server Type    : MYSQL
Target Server Version : 50610
File Encoding         : 65001

Date: 2016-01-05 23:08:20
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `sys_dict`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict` (
  `dict_id` int(11) NOT NULL AUTO_INCREMENT,
  `dict_name` varchar(256) NOT NULL,
  `dict_type` varchar(64) NOT NULL,
  `dict_remark` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`dict_id`),
  UNIQUE KEY `UK_SYS_DICT_TYPE` (`dict_type`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES ('9', '费用类型', 'moneyType', null);

-- ----------------------------
-- Table structure for `sys_dict_detail`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_detail`;
CREATE TABLE `sys_dict_detail` (
  `detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `dict_type` varchar(64) NOT NULL,
  `detail_name` varchar(256) DEFAULT NULL,
  `detail_code` varchar(32) DEFAULT NULL,
  `detail_sort` varchar(32) DEFAULT NULL,
  `detail_type` varchar(32) DEFAULT NULL,
  `detail_state` varchar(32) DEFAULT NULL,
  `detail_content` varchar(256) DEFAULT NULL,
  `detail_remark` varchar(256) DEFAULT NULL,
  `create_time` varchar(32) DEFAULT NULL,
  `create_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`detail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_dict_detail
-- ----------------------------
INSERT INTO `sys_dict_detail` VALUES ('22', 'moneyType', '收入', '2', '2', null, null, null, null, '2014-03-13 11:17:40', '1');
INSERT INTO `sys_dict_detail` VALUES ('23', 'moneyType', '支出', '1', '1', null, null, null, null, '2014-03-13 11:17:50', '1');
INSERT INTO `sys_dict_detail` VALUES ('24', 'moneyType', '预算', '3', '3', null, null, null, null, '2014-03-13 11:18:20', '1');

-- ----------------------------
-- Table structure for `sys_user`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `realname` varchar(32) DEFAULT NULL,
  `state` varchar(32) DEFAULT NULL,
  `endtime` varchar(32) DEFAULT NULL,
  `tel` varchar(32) DEFAULT NULL,
  `address` varchar(32) DEFAULT NULL,
  `create_id` int(11) DEFAULT '0',
  `create_time` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', 'admin123', null, null, null, null, null, '1', '2014-02-27 16:26:46');
INSERT INTO `sys_user` VALUES ('2', 'test', '123456', 'test', null, null, null, null, '1', '2014-02-27 16:26:46');

-- ----------------------------
-- Table structure for `tb_contact`
-- ----------------------------
DROP TABLE IF EXISTS `tb_contact`;
CREATE TABLE `tb_contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `email` varchar(32) DEFAULT NULL,
  `addr` varchar(256) DEFAULT NULL,
  `birthday` varchar(32) DEFAULT NULL,
  `remark` varchar(256) DEFAULT NULL,
  `create_time` varchar(32) DEFAULT NULL,
  `create_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_contact
-- ----------------------------

-- ----------------------------
-- Table structure for `tb_money`
-- ----------------------------
DROP TABLE IF EXISTS `tb_money`;
CREATE TABLE `tb_money` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `amount` decimal(8,2) DEFAULT NULL,
  `type` varchar(64) NOT NULL,
  `remark` varchar(256) DEFAULT NULL,
  `pay_time` varchar(32) DEFAULT NULL,
  `create_time` varchar(32) DEFAULT NULL,
  `create_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_money
-- ----------------------------
INSERT INTO `tb_money` VALUES ('57', '1', '结婚预算', '100000.00', '24', null, '2014-03-01', '2014-03-01 17:42:41', '1');
INSERT INTO `tb_money` VALUES ('58', '2', '装修预算', '150000.00', '24', null, '2014-03-01', '2014-03-01 17:42:41', '1');
INSERT INTO `tb_money` VALUES ('59', '1', '礼花、鞭炮', '5000.00', '23', null, '2014-04-26', '2014-04-27 17:42:41', '1');
INSERT INTO `tb_money` VALUES ('60', '2', '装修费', '33000.00', '23', null, '2014-03-10', '2014-04-03 22:00:39', '1');
INSERT INTO `tb_money` VALUES ('61', '1', '结婚照', '4200.00', '23', null, '2013-04-01', '2014-04-03 22:04:50', '1');
INSERT INTO `tb_money` VALUES ('62', '2', '木地板', '3000.00', '23', null, '2014-03-22', '2014-04-03 22:16:28', '1');
INSERT INTO `tb_money` VALUES ('63', '1', '新娘服装', '2500.00', '23', null, '2014-03-30', '2014-04-03 21:57:11', '1');
INSERT INTO `tb_money` VALUES ('64', '1', '新郎服装', '2500.00', '23', null, '2014-04-05', '2014-04-05 20:20:25', '1');
INSERT INTO `tb_money` VALUES ('66', '2', '燃气灶', '3200.00', '23', null, '2014-04-22', '2014-04-23 19:18:05', '1');
INSERT INTO `tb_money` VALUES ('67', '2', '木门', '5000.00', '23', null, '2014-04-22', '2014-04-23 19:18:33', '1');
INSERT INTO `tb_money` VALUES ('69', '1', '新郎母亲服装', '2000.00', '23', null, '2013-04-04', '2014-04-27 17:52:02', '1');
INSERT INTO `tb_money` VALUES ('70', '1', '饮料、烟、酒', '8000.00', '23', null, '2014-04-30', '2014-04-29 21:32:49', '1');
INSERT INTO `tb_money` VALUES ('71', '1', '新娘婚庆', '20000.00', '23', '余款', '2014-05-01', '2014-05-02 13:45:51', '1');
INSERT INTO `tb_money` VALUES ('72', '1', '新娘酒席', '5000.00', '23', null, '2014-05-01', '2014-05-02 13:46:19', '1');
INSERT INTO `tb_money` VALUES ('73', '1', '份子钱_新郎', '40000.00', '22', null, '2014-05-01', '2014-05-02 13:48:16', '1');
INSERT INTO `tb_money` VALUES ('74', '1', '份子钱_新娘', '40000.00', '22', null, '2014-05-01', '2014-05-02 13:49:12', '1');
INSERT INTO `tb_money` VALUES ('77', '2', '水电', '7000.00', '23', null, '2014-05-16', '2014-05-18 22:02:37', '1');
INSERT INTO `tb_money` VALUES ('78', '2', '马桶', '1100.00', '23', null, '2014-05-21', '2014-05-21 18:04:05', '1');
INSERT INTO `tb_money` VALUES ('79', '2', '家具', '30000.00', '23', '', '2014-05-24', '2014-05-25 12:34:42', '1');
INSERT INTO `tb_money` VALUES ('80', '2', '瓷砖', '6107.00', '23', '', '2014-05-13', '2014-05-13 18:11:59', '1');
INSERT INTO `tb_money` VALUES ('81', '2', '橱柜', '7300.00', '23', null, '2014-05-26', '2014-05-27 10:51:34', '1');
INSERT INTO `tb_money` VALUES ('82', '2', '大理石', '1000.00', '23', null, '2014-05-28', '2014-05-28 20:16:07', '1');

-- ----------------------------
-- Table structure for `tb_project`
-- ----------------------------
DROP TABLE IF EXISTS `tb_project`;
CREATE TABLE `tb_project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `type` varchar(64) NOT NULL,
  `remark` varchar(256) DEFAULT NULL,
  `create_time` varchar(32) DEFAULT NULL,
  `create_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_project
-- ----------------------------
INSERT INTO `tb_project` VALUES ('1', '结婚', '1', '', '2014-03-30 16:47:21', '1');
INSERT INTO `tb_project` VALUES ('2', '装修', '1', '装修', '2014-03-30 16:47:42', '1');
